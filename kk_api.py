import json
import requests

## Функция получения токена
def get_token(url, username, password):
    headers = {"Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
    }

    body = "grant_type=password&username=" + username + \
           "&password=" + password + \
           "&client_id=admin-cli" + "&auth_time=10000"

    token_dirty = requests.post(url + 'auth/realms/master/protocol/openid-connect/token',
                body,
                headers=headers,
                verify=False
    )

    token_clean = json.loads(token_dirty.content)['access_token']
    return token_clean

## Функция получения списка ролей
def get_all_roles_of_client(url, client_id, realm, token):
    token = 'Bearer ' + token
    headers = {"Authorization": token
    }
    clients_roles = requests.get(url + 'auth/admin/realms/' + realm + '/clients/' + client_id + '/roles', 
                headers=headers,
                verify=False)

    ## Добавление контента по клиенту в список
    roles_list = []
    roles_list_dirty = json.loads(clients_roles.content)  

    ## Создание списка ролей из полученного контента
    for data in roles_list_dirty:
        roles_list.append(data['name'])

    return roles_list

## Функция получения пользователей, имеющих роль
def get_all_users_in_role(url, client_id, realm, token, role):
    token = 'Bearer ' + token
    headers = {"Authorization": token
    }
    users = requests.get(url + 'auth/admin/realms/' + realm + '/clients/' + client_id + '/roles/' + role + '/users?max=5000', 
                headers=headers,
                verify=False)

    ## Получение контента по пользователям
    id_list = {}
    id_list_dirty = json.loads(users.content)

    ## Создание списка пользователей в формате username : ФИО
    for item in id_list_dirty:
        dict = {}
        user = item['username']
        try:
            tmp_list = item["attributes"]["LDAP_ENTRY_DN"][0].split(',')
            dict[user] = tmp_list[0][3:]  
            id_list[user] = dict[user] 
        except:
            id_list[user] = "ФИО отсуствует в LDAP"       

    return id_list
