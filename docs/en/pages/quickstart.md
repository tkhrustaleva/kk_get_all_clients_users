## Preparation and launch

To run the script, do the following:

1) Get a login and password from the administrator account;

2) Run the script and pass in the arguments: a link to Keycloak, login, password, client_id, realm:

```
python3 main.py https://keycloak.mybank.ru/ admin ZjfigmslsdoiC ndk0e937-0sl1-gj1l-so2l-3d9d9e0dkcac cicd-realm
```

3) The results will be saved in the current folder, in the `result.json` file.

```json
{
  "begemotova-oa": "Begemotova Oksana Andreevna",
  "davidov-vd": "Davydov Victor Denisovich",
  "chalkina-ie@rf.rshbank.ru": "Chalkina Inna Evgenievna"
}
```

