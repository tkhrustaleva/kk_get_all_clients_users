## Commands

Setting up `venv` and downloading dependencies:
```
python3 -m venv .keycloak --prompt KeyCloak
source .keycloack/bin/activate
python3 -m pip install -r requirements.txt
```

If a new dependency is added:
```
python3 -m pip install -U requests
pip freeze > requirements.txt
```

