## Script to unload Keycloak client users

In `Keycloak` Realms contain Users and Clients.

Both Users and Clients can be called realm-wide entities. It follows that we cannot assign a specific user to the Client.

However, Clients and Users communicate through Roles. Therefore, this script is built according to the following algorithm:

1) Getting a token;

2) Obtaining the Roles of a certain Client;

3) Users are taken for each Role in the cycle;

4) Users are added to `Python` dictionary `*`;

5) The dictionary is converted and saved into a `JSON` file.

> `*` at this stage, the feature of `Python` and programming in general is used.
>
> Dictionaries contain pairs: key - value, and dictionaries cannot contain the same keys.
>
> That is, by adding users to the dictionary, we get rid of the problem of duplication.

