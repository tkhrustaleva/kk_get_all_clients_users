## Подготовка и запуск

Чтобы запустить скрипт, выполните следующие действия: 

1) Получите логин и пароль от администраторской УЗ;

2) Запустите скрипт и передайте в аргументы: ссылку на Keycloak, login, password, client_id, realm:

```
python3 main.py https://keycloak.mybank.ru/ admin ZjfigmslsdoiC ndk0e937-0sl1-gj1l-so2l-3d9d9e0dkcac cicd-realm
```

3) Результаты сохранятся в текущей папке, в файле `result.json`.

```json
{
  "begemotova-oa": "Бегемотова Оксана Андреевна",
  "davidov-vd": "Давыдов Виктор Денисович",
  "chalkina-ie@rf.rshbank.ru": "Чалкина Инна Евгеньевна"
}
```