## Команды

Настройка `venv` и скачивание зависимостей:
```
python3 -m venv .keycloak --prompt KeyCloak
source .keycloack/bin/activate
python3 -m pip install -r requirements.txt
```

Если добавлена новая зависимость:
```
python3 -m pip install -U requests
pip freeze > requirements.txt
```
