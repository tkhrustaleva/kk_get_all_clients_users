## Скрипт для выгрузки пользователей клиента Keycloak

[Документация](https://tkhrustaleva.gitlab.io/kk_get_all_clients_users/#/README)

## Команды

Настройка `venv` и скачивание зависимостей:
```
python3 -m venv .keycloak --prompt Keycloak
source .Keycloak/bin/activate
python3 -m pip install -r requirements.txt
```

Если добавлена новая зависимость:
```
python3 -m pip install -U requests
pip freeze > requirements.txt
```

## Подготовка и запуск

Чтобы запустить скрипт, выполните следующие дейтсвия: 

1) Получите логин и пароль от администраторской УЗ;

2) Запустите скрипт и передайте в аргументы: ссылку на Keyclok, login, password, client_id, realm:

```
python3 main.py https://keycloak.mybank.ru/ admin ZjfigmslsdoiC ndk0e937-0sl1-gj1l-so2l-3d9d9e0dkcac cicd-realm
```

3) Результаты сохранятся в текущей папке, в файле results.json.

