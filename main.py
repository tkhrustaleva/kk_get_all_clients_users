import urllib3
import sys
from kk_api import *

urllib3.disable_warnings()

def get_users_of_client(url, username, password, client_id = 'cbd0e577-a9be-4f14-a592-39669e30acac', realm = 'platform'):

    dict_of_users = {}

    ## Вызов функций по получению токена и списка ролей
    token = get_token(url, username, password)
    print(realm)
    roles = get_all_roles_of_client(url, client_id, realm, token)


    for role in roles:
        users = get_all_users_in_role(url, client_id, realm, token, role)
        for key, value in users.items():
            dict_of_users[key] = value

    ## Сохранение результатов в json-файл
    with open('result.json', 'w') as file:
        file.write(str(dict_of_users))

## Запуск программы
get_users_of_client(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

